---
title: design for the new
period: 22 April - 17 May
date: 2019-01-21 12:00:00
term: 3
published: true
---

![]({{site.baseurl}}/futures-cone.png)

*Above illustration:* a quick sketch diagram on 'Design for the New' course process: identifying and understanding what the current project it is and what it could be projected as in the future. Dissecting the project into skills, images and stuff allows for understanding what exact behaviours and routines the project has to change and how this would occur in a daily scenario.

## current practice analysis

![]({{site.baseurl}}/current-practice-analysis.png)

*Above illustration:* current and desired practice map of meal preparation and consumption. The current practice map of meal preparation and consumption is a breakdown of an action in which we often perceive as simple. Yet, once breaking down this act into skills, images and stuff, it is possible to see how complex a behaviour food preparation can be - something which has evolved throughout humanity. Further, each image, will be immensely personal to each individual, in which an image of having a meal may be going out for dinner, getting a take away, having dinner alone, or with friends. Our 'skills' relate to this - we may be a chef, or have a friend who is a chef, therefore preparation of a meal may be much more complex than a person who does not yet know how to cook or does not enjoy cooking. The 'stuff' we have will also affect our food behaviour, whether we live in a shared flat with a small kitchen and limited amount of gadgets, or vice versa. In breaking this behaviour down, it is possible to distinguish but also find linking factors between each node. For example, in thinking about changing the nutritional level of an individuals diet on a day-by-day basis, it would make sense to understand the context in which they are living in. If they are solitary, they may not enjoy cooking alone, in which a 'supper club' could be set up. Another individual may not have the skillset of knowing basic methodologies or recipes, in which a recipe sharing scheme (collective cookbook) would be beneficial.

## desired practice analysis

![]({{site.baseurl}}/design-for-the-new-desired.png)

*Above illustration:* desired practice analysis. The desired practice allowed more freedom of expression and a little speculation on what the project could become. For example, for me, I would vastly enjoy the image of a 'food lab' - a converted disused space which is retrofitted with different growing technologies, all running experiments on food production within the city. The lab would also be divided into different areas, to encompass a multidisciplinary space and relink our love for food from the very seed it grows from. This would mean the incorporation of a kitchen within the space, with open areas for eating and sharing food. The food lab would provide an education on provenance and importance of fresh, healthy meals for our bodies and to punctuate the day with morsels to eagerly anticipate.
In creating this space within the urban context, I hope an awareness would be created around the timescales and care in which it takes to grow different produce. This contributes towards creating a language which challenges the cheap, throwaway culture which has been created around 'fast food' today - fast food can still be enjoyed here, but still treated with respect rather than acting from rash decisions. The food lab also speaks of an element of life which can be enjoyed and shared, as well as being nutritious.

## intervention transition pathway

![]({{site.baseurl}}/intervention-pathway.png)

*Above illustration:* intervention transition pathway to desired practice. The above illustration reflects how my current interventions will help build the foundational structure for my desired practice analysis. For example, in looking at urban food production, the two different interventions of BIOTOPIA (EAT) Festival and the Creative Food Cycles, it is possible to see how different grow systems could be implemented in urban spaces. Feedback from both workshops emphasised heavily on implementing aeroponics and hydroponics in schools, something in which I can take further in approaching schools and asking whether they would like either: a) workshops on hydroponics / aeroponics or b) having a system installed in their school in which children take responsibility in the plant they are growing, beginning to understand the fundamentals of growth from an early age. Further, at BIOTOPIA, the public were able to taste produce which had been grown in the system (basil pesto) - this allowed a relationship to form between understanding and not being afraid of a system which looks incredibly technical.

![]({{site.baseurl}}/biotopia.JPG)

## intervention prototype + learning process

*prototype: menu + then the dinner*

![]({{site.baseurl}}/bread.jpg)

*prototype: cookbook / manifesto*

![]({{site.baseurl}}/cookbook.jpg)

The 'transitional dinner' was an intervention which considered how a meal would look like without being focussed on the consumption of meats. The dinner was set in a restaurant, an authentic setting in which allowed a casual dining atmosphere despite the participants knowing they were in some kind of experiment. The dinner menu was themed around insects, in an attempt to understand the viability of insects as a nutritional composite in dishes (due to their relatively tiny environmental footprint in comparison to other livestock farming), yet making the meals delicious too. The transitional dinner was provocative in thoughts around behaviour change and how we could imagine more sustainable food choices in a dish - in limitations of diet and availability, come creativity in the way a food is cooked, presented, and paired with other ingredients. Future dinners would act as a way of developing behaviour change to a new cultural defined norm of food consumption, whether it be eating seasonal, to discovering new ingredient which are edible in the urban landscape. The creation of the 'cookbook' further encourages this - the cookbook will publish a series of different tools for food production and consumption, including recipes which have been developed during the process.

# in-class intervention rehearsal

The in-class rehearsal at first felt a little ridiculous - more in my own personal expression as I was unsure of its effectiveness, especially with passers-by giggling whilst peering  into our classroom watching everyone in role play. However, I stand corrected in the role of 'role play' and design, not only did it lighten the mood, but also used the idea of playfulness as a design response in future scenarios of our project. Despite the 'good to trash' scenario not focussed on my final project particularly, it was beneficial to see the potential future scenario sin which Nico's fungi remediator could be used, and further, be aware of the potential behaviours it may fail to change. For example, increasing the consumption of toxic materials due to their perceived 'beneficial' effects for the environment rather than reducing their consumption.  

# new version of intervention

*analogue experience at the exhibition:*

The future menu // cookbook.

In which participants answer a series of questions about how much time they have, where they live and what kind of dishes they usually enjoy. This would either be digital or through a series of 'tarot' cards which then suggest a personalised menu (which also includes the production and narrative) of the food which they will eat.
