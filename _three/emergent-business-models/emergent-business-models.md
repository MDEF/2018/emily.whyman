---
title: emergent business models
period: 16 May - 30 May
date: 2019-01-21 12:00:00
term: 3
published: true
---

## defining impact variable

![]({{site.baseurl}}/1.svg)

![]({{site.baseurl}}/2.svg)

![]({{site.baseurl}}/3.svg)

![]({{site.baseurl}}/4.svg)

![]({{site.baseurl}}/5.svg)


*Above slides*: the above slides analyse current state of the art interventions, products and platforms which exist to change our experience and behaviours towards food. In doing this, I was aware of the variety of different projects which are all working to change food behaviours but in different forms. This has helped to identify possible futures of the project, in which I find the tangible, human experience the most engaging.

![]({{site.baseurl}}/future-times.svg)

*Above image*: the future times image is a detailed description of how the Collective Edibles project would develop in the near future. Collective Edibles would progress into a physical lab, identifying potential partners through existing infrastructures, such as IAAC and Fab Lab. Identifying what currently exists and how my project could fit into the views and existing projects of these infrastructures will be a very effective way of gaining momentum and support base in a quicker time frame than initiating from the start.

## sustainability model

Below, will describe a projected sustainability model in which the project could financially sustain itself: there are a variety of different options touched upon here.

### open-source
The open-source movement promotes itself on files which can be downloaded and made yourself online. The project is, and will continue to be open source, however there are elements which could be sold as a kit if a customer did not want to make the product themselves. This can be done with the mealworm kit, and future editions of the kit. For example, future editions intend on incorporating mealworms and plant growth (hydroponics or aeroponics) together. The project would be similar to the vertical farming platforms of [Growstack](https://wikifactory.com/+growstack) and [Nextfood](https://nextfood.co/). Growstack is completely open-source aeroponics and Nextfood uses Growstack technology, selling their software, expertise and maintenance.

### subscriptions
Another method of gaining profit from the project would be to remain open-source but sell a subscription to the platform or maintenance in which to promote products. For example, a restaurant may develop a delicious cookie in which they would like to sell in batch as a sweet-treat protein pack. The platform could then be a place for exchange - in which the restaurant is subscribed to.

### events
Experience or ‘transitional’ dinners could be part of the sustainability model, in which restaurants have host evenings in which are offer events similar to that of the LEKA dinner. This would be a service provided by Collective Edibles - however in partnership with the chefs and hosting restaurant as a way of promoting their sustainable image and curiosity.

### promotion
Promotion of these events would be via the website, social media channels and through platforms such as [Tripadvisor](https://www.tripadvisor.com/). Once a reputation has been gained on Tripadvisor, the project would sustain itself and ideally find more clients in its exclusivity and difference to the norm.

### wider context
The wider context of the project relates to ongoing socio-political context and the increasing topic of climate change. The issue of climate change and our behaviours will continue to be a theme within politics and the media - therefore the project will play a role in voicing opinion about how we implement changes in our diet. This project further fits comfortably in the vision of the [United Nations Sustainable Development Goals](https://www.un.org/sustainabledevelopment/sustainable-development-goals/), with the second goal of ‘Zero Hunger’ - this includes that of nutritional value - the third goal being ‘Good Health and Wellbeing’. This project also fits into more of the goals, with those regarding saving water, promoting sustainable cities and food futures.
