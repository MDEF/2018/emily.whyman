---
layout: page
title: experiments
permalink: /experiments/
published: true
---

# aeroponics

 ![]({{site.baseurl}}/aeroponics/germination.jpg)

 ![]({{site.baseurl}}/aeroponics/cress-2.jpg)

 ![]({{site.baseurl}}/aeroponics/strawberry-light.jpg)

 ![]({{site.baseurl}}/aeroponics/two-shelves.jpg)

# fermentation

![]({{site.baseurl}}/images/ginger beer/scoby.jpg)

![]({{site.baseurl}}/images/ginger beer/ginger.jpg)

![]({{site.baseurl}}/images/ginger beer/chopped-ginger2.jpg)

![]({{site.baseurl}}/images/ginger beer/chopped-ginger.jpg)

![]({{site.baseurl}}/images/ginger beer/adding-sugar.jpg)

![]({{site.baseurl}}/images/ginger beer/stirring-jars.jpg)

![]({{site.baseurl}}/images/ginger beer/ginger-buck.jpg)

![]({{site.baseurl}}/images/ginger beer/ginger-beer.jpg)

![]({{site.baseurl}}/images/ginger beer/ginger-beer2.jpg)

## kombucha

![]({{site.baseurl}}/images/kombucha/tea.jpg)

![]({{site.baseurl}}/images/kombucha/kombucha1.jpg)

§  for more weird and wonderful ideas check out my [are.na](https://www.are.na/emily-whyman)
