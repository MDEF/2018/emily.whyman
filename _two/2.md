---
layout: two
title: 2
permalink: /2/
---

![]({{site.baseurl}}/bioplastic.gif)

## 01 - [design studio](https://emilywhyman.gitlab.io/collective-edibles/)

## 02 - [fab academy](https://emilywhyman.gitlab.io/fab-academy)

## 03 - [material driven design]({{site.baseurl}}/two/material-driven-design/)

## 04 - [atlas of weak signals]({{site.baseurl}}/two/atlas-of-weak-signals/)
