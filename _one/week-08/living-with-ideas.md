---
title: Living with Ideas
period: 19-25 November 2018
date: 2018-11-19 12:00:00
term: 1
published: true
---



## “Design becomes a producer of sense. To be more precise: to the question “what does design do?”, the new answer is: “It collaborates actively and proactively in the social construction of meaning.”
Ezio Manzini.


## meaning

*A reflection on the agency of things, people, objects, actions.*.

Why do we do the things we do? What instigates our behaviours and establishes trends, however ridiculous they may seem? Understanding the fundamentals of our behaviours is an abstract concept we have been contemplating for centuries - we have even started to deflect these behaviours into intelligent objects before we have a comprehensive understanding. It is by far no easy feat to become the next *Lefebvre* or *Latour* overnight, however we can begin to understand paradigm shifts through investigating the agency of the macro built environment, to the micro agencies of timescales and time cycles. This echoes actor-network theory's material-semiotic method of understanding space - how do we *communicate* with objects, things and surroundings?
What is the *language* of our built environment? How do things communicate with us in space? I say *things*, as it is difficult to pin-point exactly what we are communicating with and when - we don't *speak* to buildings or to trees in the way we do with other humans, but we communicate with them in forms which we perhaps do not realise - overlooking this has led to some of the wasteful habits in place today.

## provoking results  

The week commenced with questioning how we can interact with familiar objects we currently *think* we understand. When we speculate about these objects we question their agency - what if these objects actually questioned our thought processes regarding the world and our habitual routines?
After picking a tote bag for recycling from the assemblage of different objects on the table, the aim was then to speculate these objects: how can we change the agency and meaning of these objects to provoke a reaction?
Our inner-child prevailed here, producing hairbrushes designed for orbital explorations and x-ray vision glasses.. So if this tote bag was not a bag, what could it be?

An umbrella?
A cushion?
A jellyfish?
What if we did not have to carry anything because the world had become completely digitalised?
Why do the majority of tote bags display some form of branding?
What message does it communicate?
How do we label society?

After studying the tote bag in detail and eliminating some of the more ludicrous ideas, the typeface was chosen as the most interesting feature to be subject to speculation. Through dissecting each printed letter on the bag, I began to question our use of printed language and slogans in our society. Could we question the existence of language, speech and intelligence through speculation?

## what if we could only communicate written language through words which have already been printed?

Perhaps one day deforestation will have engulfed too many of the worlds resources and felling trees is no longer a viable option, typography printing becomes a rare and collectable resource. How would we communicate through the written language? Would we *recycle* letters? In which other forms would we communicate and express deeper meanings to our sentences? Through colour, or speech? What would occur to typefaces, slogans and communication? How would we 'brand' ourselves? Creating these kooky sentences adhered within a sketchbook allowed exploration and appreciation of communication we take for granted - the ability to articulate ourselves with the written word.


{% figure caption: "" %}
![]({{site.baseurl}}/speculative.jpg)
{% endfigure %}


## changing skin

"If you could wear fabric that acted like a computer, or, fabric that could change dynamically. When, where, why would you wear it?"

Dynamic fabric offers a different conception of how materials can behave. What if fabric could confuse surveillance mechanisms, allowing anonymity? What if it could sense your mood, altering it's colour and pattern to suit your desires? Or what if it could act as a safety mechanism, lighting up when the day darkens?

 ![]({{site.baseurl}}/jess.jpg)

After a comedic 30 minutes of parading around the room wearing different vibrantly coloured materials and capturing them through [Veescope Live,](https://itunes.apple.com/us/app/veescope-live-green-screen-app/id568390801?mt=8) our outfits had transformed into projections of landscapes, different colours, and pizza. Perhaps we did not have enough time to explore this technology fully, however interesting proposals were derived through the disappearance acts, and pizza capes.

The change in the agency of the materials offered a unique way of communication, through projection and storytelling. There lies opportunity in the relationships dynamic fabric *could* create. Perhaps it will be possible to perform biomimicry through what we wear, encouraging biodiversity and symbiosis. Mimicking the essence of what the fabric *could be* superseded potential creative 'blocks', allowing the expression of the full potential to change the meaning of the clothes.

![]({{site.baseurl}}/nico.jpg)
![]({{site.baseurl}}/nico2.jpg)
![]({{site.baseurl}}/nico3.jpg)


![]({{site.baseurl}}/line copy.jpg)

## “By the creation of imaginary worlds, and by designing fictions, we actually question the world we live in – its values, functions, its metabolism, as well as the expectations of its inhabitants.”

[Speculative Design Practice.](http://speculative.hr/en/introduction-to-speculative-design-practice/)

![]({{site.baseurl}}/line copy.jpg)

## magic machines

"Make a magic machine?"

"A what?"

"A *magic* machine.."

Curiosity: defined as 'the need to gain knowledge.'

Introducing the ‘Dinner of Curious Delights’. Desires: Through delving into your more curious side, and opening the dinner of curious delights, you will be offered a variety of different ingredients in which feed the inquisitive mind, developing knowledge. The magic? Consumable, instant knowledge, fast.

It is difficult to define specific behaviours or actions which develop intelligence. Curiosity, or inquisitiveness, definitely acts as a catalyst for absorption of knowledge, however different forms of ‘intelligence’ may be obtained. For example, emotional intelligence may be developed through experience, whereas education is fed through schooling. Hence the ‘dinner of curious delights’ - a magic machine for instant-absorption of intelligence, without having to learn by trial and error. The creation of this magic machine questioned our social construct: *why* do we need to gain knowledge, *how* do we do it? Is knowledge relative to trends?

![]({{site.baseurl}}/magicmachine.jpg)
![]({{site.baseurl}}/dinner2.jpg)

## (invisible) thresholds

Our discussions throughout the week led to ultimate questioning of the human condition and some philosophical thinking relating to time and scale. If we didn't have the concept time how would our rhythms and cycles change? We may start off by running around like headless chickens, frantic around the fact we may be late or early, missing those all too important work calls and meetings. However, we may actually adjust to a more *natural* rhythm. The concept of a *trend* may cease to exist - what would 'trash' be if we didn't have the behaviours which label still-functioning objects as dated and unfashionable?  Echoing fractals, these self-replicating behaviours can be seen graphically through fractals - the reproduction of societal constructs and fast-consumption habits which are consummated with the basis of *time*.

## the new normal

What is the new normal? What even is *normal?* Getting slightly lost in the world of meaning and social constructs, I found myself in a hazy blur of questioning everything I know and believe in. Returning to tangible reality, I began to study our everyday patterns through recording different scenarios: from the classroom, to the cycle home. Through watching the small-amount of footage, it is evident how our functioning depends on the cues we take from our environment. When these are interrupted, we produce unexpected responses. A near-miss on the bike with an idling sightseer highlighted the contrast between living in a city, and visiting it. We mostly choose to overlook something, or *somebody* which contradicts these norms - the vast amount of displaced individuals is evident when studying the streets of Barcelona, however their presence has now become a social normality, objectified to the pavements as another token of society, a reminder of the darker, more ruthless side of a capitalist environment. Perhaps this is easier termed as 'The Invisible Rulebook of Social Norms'. Is it possible to disrupt these invisible 'rules' we have in society in order to implement change? Could it create awareness around wasteful behaviours and potential uses of space?

![]({{site.baseurl}}/book.jpg)

## afterthoughts..

As I was running this morning, I heard an overwhelming clamour of freighter horns, all leading out form the port. Unusual, I thought. Or maybe they were slightly miffed off about spending hours trying to get out of the port. Returning an hour later, the trend of the horns was still continuing, not to mention that this was at 08:00am. The horns may have ruined someones lazy morning, however they had attracted quite the audience of curious inspectors, peering over the walls to the road to nosey about what was occurring. Still none the clearer about why this was occurring, I carried on home. However, through breaking their invisible presence in society, I was reminded of how we rely on goods transport and the sheer amount of items which are now carried around the world by these silent carriers..

## food for thought

[Speculative Urbanism.](https://thenewnormal.strelka.com/)

[Design for the New Normal](http://superflux.in/index.php/work/design-new-normal/#)

[Data Visualisation Using Household Objects.](https://www.vice.com/en_au/article/xy4dw7/infographics-for-the-crafty-data-visualizations-using-household-objects)

[What is Cosmopolitical Design?](https://www.routledge.com/What-Is-Cosmopolitical-Design-Design-Nature-and-the-Built-Environment/Yaneva-Zaera-Polo/p/book/9781472452252)

[Making the Social Hold: Towards an Actor-Network Theory of Design](https://www.tandfonline.com/doi/abs/10.1080/17547075.2009.11643291)

[Designing for the Inbetween.](https://medium.com/s/world-wide-wtf/designing-for-the-in-between-hybrids-1990s-net-art-and-a-giant-floating-worm-34be64b872d3)

[Giant Hands Emerge from a Venice Canal to Raise Climate Change Awareness.](https://www.vice.com/en_au/article/d7akxv/giant-hands-emerge-from-a-venice-canal-to-raise-climate-change-awareness-creators)
