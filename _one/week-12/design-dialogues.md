---
title: Design Dialogues
period: 17-23 December 2018
date: 2018-12-23 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/ollie.jpg)

# honest food

Compiling the material for the exhibition itself acted as an exercise to illustrate how much information we have absorbed over the past 12 weeks. Rather sponge-like, it would be very easy to blurt out random facts about microbes, or electronics, however a little more difficult to synthesise our topics into our thoughts around a final project. Creating the 'Honest Food Market' highlighted my broad contextual knowledge and beliefs about the current food system, but failed to delve directly into a specific manifesto which I am trying to convey.

![]({{site.baseurl}}/fruit.jpg)

![]({{site.baseurl}}/layout3.jpg)


# things to chew on

Feedback. The feedback offered a variety of different interesting directions to follow. Not only did I receive critical feedback on my topic, but more overarching comments: these related to finding the specific focus of where my intervention can be - which specific elements of urban agriculture am I interested in and wish to pursue? What is unethical and misguiding about our current food systems? There is the need to start a narrative of this project - from it's initial baby steps to the grand finale. To do: start videoing (creating a narrative); [Obvious Plant](https://www.instagram.com/obviousplant/]) inspiration (questioning distribution channels); map current food initiatives in Barcelona (contextual state of the art); provide apocalyptic future scenario; routines of current growing systems; index of what can be grown in Barcelona.

![]({{site.baseurl}}/layout.jpg)

![]({{site.baseurl}}/layout2.jpg)

# food futures

I will attempt to break down these tasks for the following 6 months, constantly testing and prototyping different ideas in which will guide the following iterations of my project and the field which I wish to study and contribute towards. Different options:


Create an IAAC vending machine, start IAAC food community and manifesto.

Alternative food installation at Viva food festival, awareness about habits.

Create a connection between UA initiatives in Barcelona. What are the tools to make people communicate with each other and also create awareness around the existence of these initiatives?

Create a map of where the different products come from different chinos in Barcelona.

Education methodology around locally grown food.

Veg box scheme, local kickstarter with the informal settlements.

![]({{site.baseurl}}/poster 2.jpg)

## inspiration

[Human Habitat's](http://www.humanhabitat.dk/projects/) Impact Farm

[Obvious Plant](https://www.instagram.com/obviousplant/)

[Open Eat Festival](https://www.biotopia.net/en/event/biotopiafestival-en)

[Nextfood](https://nextfood.co/)
