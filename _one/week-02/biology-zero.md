---
title: Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/Discussions around what found.jpg)


I soon realised that the title ‘biology zero’ was an incredibly apt name for the week. From day one, to day five, we encompassed an enormous range of topics - whether pigs could *actually fly* to learning how to grow our own super-food: spirulina. All the strange and wonderful discussions and experiments we completed spurred a renewed vision of *how* we view the environment and world through design - through acknowledging the roles and responsibility of the designer to create an environment of **transparency.**

{% figure caption: "bioluminescent bacteria" %}
![]({{site.baseurl}}/bioluminescence.jpg)
{% endfigure %}

## biologists are designers

The ability to splice and modify genes is a unnerving thought, therefore epigenetics causes great reason for discussion and debate. However, it can be viewed in the sense that biologists are also designers - designers which on a microscopic scale which have formulated many things we use and consume today. My original view on this topic resided within a field against altering genes, finding it an unnatural man-made method of altering nature which may provide unpredictable results. Nuria however, illustrated how we now live in a world of the **Anthropocene** - the historic domination of mankind on the natural environment has altered it so much that we have created a new era in which climate change and increasing populations are our threats. There will always be negative side-effects of genetic engineering, however there are also many other possible negative side-effects of many other supported procedures. Genetic engineering therefore creates hope in the possibility of altering what is **biologically possible** and what this means towards production and consumption. For example, if a crop can be modified to require less water in its growing stages then why would this not be used in future agricultural production? This also relates to the **responsibility** of the designer and **transparency** of the design process. The current climate of distrust towards new biological technologies relates to the control and influence  of multibillion dollar companies over the biology industry.

{% figure caption: "understanding wavelengths" %}
![]({{site.baseurl}}/drawing diagram.jpg)
{% endfigure %}

### materials matter

Through understanding the world at a microscopic scale, designers are given new interfaces to play with - increasing our tangibility and complexity of understanding the process of design. The current linear design process encourages designers to solely consider the **end product**, ignoring the exhaustive product development process which is often ecologically damaging and unethical. The talks with [Mette](http://materialdesignlab.dk/) and [Thora](https://www.instagram.com/bio.babes/) returns to the need for design to become transparent. The idea that mushrooms can create structural stability  yet can also decompose is an exciting prospect in the movement towards **circular design**. Nature can now be reinvented within design: we can incorporate structures which not only encourage human growth but also growth of natural ecosystems. It’s time to change the **design dialogue**, perhaps in 20 years time, our light switch will in fact be bioluminescent bacteria and our houses will be grown from mushrooms **..**


{% figure caption: "watching our bacteria grow" %}
![]({{site.baseurl}}/taking out of incubator.jpg)
{% endfigure %}

### DIY biology : how to cultivate almost anything

*"A movement that aims to open source, tinker, and experiment with biology outside of professional settings. They see it as a grassroots movement and a platform open to everyone that builds on synthetic biology and identifies biology as a resource for tinkering or biohacking."*
*(McCarthy and Wright, 2015)*

The open-source *Biohackers* movement is a renewed effort towards taking **ownership** towards ecological damage and against capitalist corporations. We discovered how to cultivate spirulina : a super-food in terms of it’s energy requirements for production - not so much it’s taste. The hardiness of spirulina to resistance and climatic conditions, including it’s nutritional protein value offer it as a food for the future - alongside explorations into eating insects and worms: SPACE10, have their very own lab and chefs, designing the future of food.

{% figure caption: "'iron syrup' for spirulina" %}
![]({{site.baseurl}}/spirulina.jpg)
{% endfigure %}

### wavelengths and eggshells


We used spectrophotometry to show how an organism can be quantified, and stoichiometry to work out how much calcium was in an eggshell. The use of biological methodology therefore leads to an understanding of the micro elements of the world we see. This led to our own proposals, using biological methodology: proposing initial questions, a hypothesis, methodology and possible outcomes.

{% figure caption: "spectrophotometry" %}
![]({{site.baseurl}}/box again.jpg)
{% endfigure %}


### fractals in the city


Many of our discussions during the week led to Jonanthan recommending *Ron Eglash’s* lectures on [African Fractals](https://www.ted.com/talks/ron_eglash_on_african_fractals?utm_campaign=tedspread&utm_medium=referral&utm_source=tedcomshare).
**Fractals** are an infinite mathematical graphical computation, which produce and reproduce shapes which strangely resemble those which exist in nature. They are completely scalable and can be seen when looking at leaf formations, to the formation of settlements in cities. This natural, yet unnatural algorithm is fascinating when looking at the design of singular vernaculars, to cities.

Self-organising systems are also resident in bacteria, which form around the availability of nutrients and preferential growing environments - allowing their own biological cycles to replicate and expand. In researching fractals, I had inherent questions about the self-organising structures of the urban environment and how it was influenced: could fractals be applied to understand social scaling? I was also curious as to how urban expansion and development was affecting the organic systems of the city - for example, increasing tourism in Barcelona would introduce thousands more bacteria on the floor of *Las Ramblas* than in the previous decades: how does this affect the ecosystem?

{% figure caption: "[some rather beautiful african fractals](https://twitter.com/arquimaestros/status/576771914259136512)" %}
![]({{site.baseurl}}/african fractals.jpg)
{% endfigure %}

### questions


What are the self organising systems of Barcelona? (Using African fractals as a methodology).

What urban structures/rhythms are similar to the process of replication on a macro scale?

How are these processes related to conscience / synthetic biology?

Therefore, how much actions in the city do we perform without knowing?

Our environment is engineered - what system does this support?

*“The synthetic city”* **?**

### hypothesis

"Cities will necessarily become self-replicating entities if they are to evolve, fractal urbanism and bacterial division patterns are identifiers of self replication through macro / micro + organic and urban environments."

### method

1. An algorithmic computational environment will be made, programable to follow all known bacterial growth patterns.
2. A synthetic city will be formed first digitally and then by biotic computation.
3. Comparisons and a common language will be formed between all known models and patterns.
4. Create a visual mapping system which will act as a generic medium in order to compare results.
5. Study increase/decrease in urban complexity in synthetic city after 1 month: categorise what is developed - e.g. supermarkets, offices, parks etc.
6. Study increase/decrease in bacterial complexity after 1 month.
7. Identify which patterns encourage ecological diversity and natural growth.
8. Compare to a current real-time city: e.g. Barcelona (see other ‘test’).  
9. The evolution of a *‘city model’* - an ideal, diverse fractal which self replicates from small areas of the urban district to the whole city system.

Test would be run alongside an urban study of Barcelona.

1. Study a real city: e.g. Barcelona.
2. Create a visual data map for each organism. (e.g. slime mould).
3. Create a biotic index of organisms that exist in order to categorise *what* is self-replicating and *where*.
4. Choose one particular bacteria and follow it’s movement and growth through the city (e.g. slime mould, or moss).
5. Identify self-replicating non-organic elements of the city: social / economics networks.
6. Introduce a complexity of networks to the synthetic city through increasing the diversity of nodes.  
7. Test whether microorganisms follow the introduced networks through the ‘city model’ created.



### how much could we know our cities?


Through performing this test, it would be possible to create a “design guideline for non-human species” (Downton, P). Instead of destroying natural systems in the production of new cities, it would be possible to plan ecosystems into cities through understanding the self-replicating nature of fractals. For cities to evolve and provide resilience towards the threats of climate change, there needs to be a de-centralised distributed city network which resources are managed under local control. The fab labs are part of this movement for the future city. This week has reinforced the need for transparency of design processes and how design begins from the micro. I am intrigued to discover more about how to synthesise biology into design, from the power of mushrooms to experimentation with new materials.

What is the DNA of the city?

“Distributed city” concept (de-centralised, etc). The fab lab network as part of the distributed city network: resources management under local control.

Create a “design guideline for non-human species”.

“Planning and city governance facilitate the emergence of natural patterns and ecosystems within the artificial ecology of urban systems.”

{% figure caption: "[dna of the city](https://kottke.org/18/04/city-dna)" %}
![]({{site.baseurl}}/city dna.jpg)
{% endfigure %}

### food for thought

Ecopolis: Architecture and Cities for a Changing Climate
Authors: Downton, Paul F.

[Neighborhoods and Urban Fractals—The Building Blocks of Sustainable Cities – The Nature of Cities](https://www.thenatureofcities.com/2012/10/17/neighborhoods-and-urban-fractals-the-building-blocks-of-sustainable-cities/).

Taking [A]part: The Politics and Aesthetics of Participation in Experience-Centered Design: John McCarthy and Peter Wright.

[Building Ecocities](http://www.ecotecture.com/library_eco/interviews/register1a.html)

[Strategic Urban Governance](https://dac.dk/vaer-med/community/strategisk-byledelse/)

[The Distributed City Model](http://www.telecommuter.org/DCM/DCM-DCM2.html)
