---
title: An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---
![]({{site.baseurl}}/galaxy.jpg)

*“Arthur blinked at the screens and felt he was missing something important. Suddenly he realised what it was.
"Is there any tea on this spaceship?" he asked.”*

― Douglas Adams, [The Hitchhiker's Guide to the Galaxy](https://www.goodreads.com/work/quotes/3078186)

Quintessentially British, The Hitchhiker's Guide to the Galaxy outlines many potential future problems, one being a reduced yield of tea crops, and therefore tea. Perhaps if you mentioned this prospect to Britons they would rapidly change their consumption habits, since our genes are tainted with a tea-addiction from birth.

What has science fiction and tea drinking got to do with future studies? Many things - science fiction, although *fiction* in fact often portrays many advanced technologies we have proceeded to create.

# oxymorons

Unthinking the future in order to re-think the future. A phrase which is a little bit confusing for the old noggin. We look at the past in order to predict the future, however it offers glimpses of a path of humanity we are ebbing towards. George Orwell's monumental '*1984*' offers a dystopian prediction of an 'impossible' future - a future which ironically sounds a little familiar to today. The idea of constantly being watched by 'Big Brother' echoes surveillance routines used worldwide, the more extreme methods using surveillance machines as a method of assessing your actions, or restricting them for that matter. 'Black Mirror' is perhaps a modern-day visualisation of some of the scenarios predicted in *1984.* Chilling in its representation of entirely possible worlds, the audience is presented with the possible extension of interfaces which sculpt worlds, relationships, personal habits and ratings. What would George Orwell think if he watched Black Mirror?
These mental structures are narratives of the future, imagining dark scenarios which require revision. Historic depictions of the 'future' may in fact hinder humanities progression, in which we are now a mere projection of vanity, a self-fulfilled prophecy which is doomed to become an automated cyborg race.

Our perception of the future however revolves around how we class the *present.* Our creation of physical, measurable time is a human conception which other cycles continue to ignore, working to their own personal rhythm and pace. When these cycles are broken — otherwise termed as a 'black swan' event — we become aware of where we stand in a time of rapid breakthrough and advancement.

![]({{site.baseurl}}/1984.jpg)


# what to think about the future

How do we think about the future without actually *thinking* about the future? i.e. How do we avoid these narratypical stereotypes which have influenced so much of our pre-conceptions around the future? Media, particularly social media, has affected the way which we view events which occur, with knowledge of a dramatic incidence spread across digital channels faster than it could ever be printed. However, the perception of the future remains a Western dominated viewpoint which fails to view **all the possible set of scenarios which the future could hold.** [Afrofuturism](http://afrofuturism.net/) offers self-projections without the sovereignty of the Western world - returning to time cycles - Afrofuturism explores the progression of African society alongside the outlandish music of Hip Hop: the best music to argue within around social and transformational change.
Sets of 'futures' garishly sold to the public are laced with technological capacities and spread over an enormous city scale. Despite being impressive to the eye these depictions fail to acknowledge the more *human* side of future studies. Where are the scenarios which acknowledge that people may in fact miss their old 9 till 5 hectic lives in a world where no work is abundant? What would happen to their concept of time, money and life in general?

![]({{site.baseurl}}/afrofuturism.jpg)


# micro-utopias

Continuing my interests regarding growing systems networked through the city on an urban scale, it is possible to visualise scenarios in which spaces can be utilised in order to help provide growing systems in the pledge to become more self-sufficient? Looking at science fiction and its predictions for food, it offers insect dishes, 3d printed food, and again, from Hitchhiker's Guide to the Galaxy - the "vegan rhino cutlet." Sounds rare and exquisite.

Within futures thinking, this could be placed in the 'transition design' category. Transition design looks at creating small networks of power, linked through cities in order to offer behavioural nudges to higher powers around further actions regarding the social welfare of citizens. Manoeuvring issues such as *automation*, transition design looks at these potentials and seeks opportunity. For example, mass reduction in cars within city centres opens up huge new obsolete expanses of area. What if these areas could be repurposed through weaving growing systems through their brutal facades? How would our cities *feel?*

{% figure caption: "[Infrastructures in Obsolescence Workshop, Chile.](https://www.archdaily.com.br/br/906746/wio-21-equipes-universitarias-imaginam-intervencoes-em-7-estruturas-obsoletas-em-santiago)" caption%}.
![]({{site.baseurl}}/obsolete.jpg)
{% endfigure %}



## food for thought

[Afrofuturism, the Genre that Made Black Panther.](https://edition.cnn.com/2018/02/12/africa/genre-behind-black-panther-afrofuturism/index.html)


[Design for Micro Utopia: Making the Unthinkable Possible, John Wood.](https://www.goodreads.com/book/show/38326984-design-for-micro-utopias?from_search=true)
