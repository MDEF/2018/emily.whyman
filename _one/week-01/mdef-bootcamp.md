---
title: MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---

{% figure caption: "*poblenou*" caption%}.
![]({{site.baseurl}}/poblenou.jpg)
{% endfigure %}

## the process

* Seminar-style discussions.

* Interdisciplinary approach.

* Exploring the creative interface of Poblenou.

* "Trash" hunt for materials.

* Reflective piece (aka this).



The pre-course for MDEF offered a glimpse of what the rest of the course is going to be like, varied in content and fast-paced. The initial discussions revealed that *'The Future Me'* lies within a job role that does not exist yet - somewhere in the future development of new technologies lies a role for us as designers. It was a relief to hear that this was a similar trend across the group, it became evident that everyone was a little unsure of *what* they wanted to delve in to but were excited at the prospect all the same. If I were asked my professional status I would stutter and suggest *'urban design'* as a quick response. I studied Architecture and Urban Planning previously, which dipped into both sides of the design world, in which I quickly realised I did not want to be an architect, nor an urban planner despite finding it interesting. The book *Architecture Depends* by Jeremy Till resides with me as an incredibly comprehensive and humorous insight into the problems of the architecture and planning traditions that exist today.



There is an informal salvage economy which runs adjacent to the more formal tourist business in Barcelona. This was evident on the *city safari*, with the dismantling of objects for their metal. Not only is there a salvage economy, but the economy of the *manteros*. The issues of mass migration which feed the informal economy are prevalent in Barcelona and can be specifically seen around the streets of Barceloneta and Las Ramblas. This informal economy is partly due to Spain's economic crash, and migration from poorer countries in the global South as a result of inequality and climate change (BBC News, 2017).


{% figure caption: "*a quick sketch on the flow of movement and materials in barcelona* caption%}.
![]({{site.baseurl}}/sketch.jpg)
{% endfigure %}


The progression of the week highlighted how we can introduce new ideas and methods of learning into design culture and immerse ourselves into an exciting movement of **open source design, alternative economies, fabrication technologies and self-sufficiency.** I see my future role as a designer to change the way we consume; the *City Safari* illustrated how the meaning of *trash* varied for each individual. In the literal sense, the trash is something in the bin, or beside it in this case. However, when viewed upon as a source of *material*, garbage-hunting suddenly became very exciting. The 'material library' of *Transfolab* gave a clear design message about how we produce and consume. The tour of Poblenou offered surprises hidden behind the ex-industrial façades of Poblenou lie - creative industries in which offer a sense of activism towards recreating and reinventing the city.



## Systems I want to investigate:

* [Aeroponics](https://mdef.gitlab.io/emily.whyman/weeks/aeroponics/aeroponics/).

* Existing urban garden collectives in Barcelona.

* The social dynamic and economy of Barcelona - how tourism has affected the local culture.

* How digital fabrication can enhance our standard of living in cities.


{% figure caption: "the beginning of the *poblenou inventory.*" %}
![]({{site.baseurl}}/poblenou inventory.jpeg)
{% endfigure %}




I want to investigate how urbanisation can be productive, in which choosing a more sustainable option does not require a sacrifice of time and effort, but in fact offers a more tangible and enjoyable way to experience urban life. Perhaps in the future, there may be such a thing as positive consumerism.

{% figure caption: "*talking brains, indissouble*" caption%}.
![]({{site.baseurl}}/brain.gif)
{% endfigure %}



## food for thought



BBC News (2017). Barcelona's 'blanket men' try to rebrand. [video] Available at: https://www.bbc.com/news/world-europe-42256346 [Accessed 7 Oct. 2018].

Pradel, M. (n.d.). Informality in Barcelona. [ebook] Available at: http://blogs.sciences-po.fr/recherche-villes/files/2014/09/Marc-Pradel_Informality-in-Barcelona.pdf [Accessed 7 Oct. 2018].

Till, J. (2013). Architecture depends, Cambridge, Mass.: MIT Press.
