---
title: The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/things.jpg)

## breaking the gadget

We tend to think of technology as a tool which will speed us up, make lives more simple, and comfortable. However, when technology fails, we are left in a dark abyss of the unknown due to our lack of education on *how* to fix things. Our items which once provided such ease to our routines have now become, essentially, a brick. The failure to apprehend a world post planned-obsolescence is not a fault of our own - we have been trained by Apple and other companies that once something breaks, either only they can fix it, or it is irreparable, doomed to be forgotten on the electronic-waste pile in a scrapyard in some country miles away. We could learn a thing or two about where our outdated and undesired electronics go, and further, how they are dismantled. The desperate families who are dangerously taking apart these objects are in-fact pioneers of a circular waste economy, yet the ethics are all wrong - take a look at [E-Waste in India.](https://news.nationalgeographic.com/news/2014/06/140628-electronics-waste-india-pictures-recycling-environment-world/)

{% figure caption: "(Photograph by Zoran Milich, Getty Images) [Each U.S. Family Trashes 400 iPhones’ Worth of E-Waste a Year.](https://news.nationalgeographic.com/2017/12/e-waste-monitor-report-glut/)" %}

![]({{site.baseurl}}/e-waste.jpg)

{% endfigure %}

Through dismantling different household objects, we were exposed to the stark reality of how many different components went in to printers, kettles, phones and more. The complexity of the current technological world has abstracted the art of 'DIY'. We only interact with the interface of components, not the assemblage which lies underneath the surface.

{% figure caption: "Taking apart an Epson printer." %}

![]({{site.baseurl}}/day1collage.jpg)

{% endfigure %}

## tangibility of things

"Hacking" the everyday object questioned the fallibility of 'the gadget' : it's embedded technologies produces niche products connected to closed-source programmes. So, how could we dismantle these products, reuse their still-functioning internal motors, drivers, DRAM's and other micro components to transform our interactions with interfaces in the everyday reality?
Continuing a fascination with the systems and cycles biological life, we questioned whether we could *make a plant talk*, or sing for that matter, particularly when it needed water. We were considering opting for a well-known, anthem of a song. *Queen*, or *Abba* to be precise. Using moisture sensors embedded in the soil, we began to experiment with LED's  which would indicate using green, or red, whether the soil was a little-too-dry for the plants liking.


## more than an interface

Introducing the rather aptly named *"Wet my Leaf"* project (comes with free stickers, too). By far the most enjoyable and rewarding part of the week, we saw our toils come to life through the connection of the sensors to the server.   When touching the plant, a sound via the compressor would be activated, or through the phone.  Through creating interfaces that were completely irrelevant to any form of meaning in everyday life, we were able to hack everyday objects using our imaginations. The week has provided invaluable reason to tinker and play with commonplace objects but also a more genuine reality, in which we can begin to repair the broken and question the diverse range of materials put into production of everyday items.

Wet my Leaf [website.](https://wetmyleaf.github.io/?fbclid=IwAR1pOMrjlmgQi9U6wCM3rQeUak2ceT68YIC7Zgngt4dlpkF40g5NfFCFYSA)


![]({{site.baseurl}}/plants.jpg)
![]({{site.baseurl}}/code.jpg)

## food for thought

[The Global E-Waste Monitor.](https://drive.google.com/file/d/11DCXXZM-bflxHxk92gOXbweIceb40brP/view)

[What Should I do with my Broken Kettle?](https://www.bbc.com/news/business-45969676)

[The Circular Economy of E-Waste in the Netherlands: Optimising Material Recycling and Energy Recovery.](https://www.hindawi.com/journals/je/2017/8984013/)
