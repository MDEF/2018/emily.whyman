---
title: Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---

## day 1 - [**Ariel Guersenzvaig**](http://www.elisava.net/en/center/professorate/ariel-guersenzvaig)

![]({{site.baseurl}}/ariel_collage.jpg)

### introductions

Our introduction to hybrid professions began with meeting Ariel, a current lecturer and researcher at ELISAVA. Originally studying Information Science, Ariel reflected on his fluid transformations of profession with the emergence of the "web" - not "internet." We are reminded of the rapid development of technology over the last decade, and the needs for designers to respond dynamically to real-time advances. Business logistics and Information Architecture may not be my forté, however it was interesting to discuss the limits and advantages of being a freelance designer vs partnership with a multinational company - this is food for thought when looking or creating a job post-masters.

I was amazed when Ariel stated his feelings of imposter syndrome taking on a new project, a relief on my behalf - I often question my position and knowledge in the design process. This is symptomatic of a 'learning by doing' design process however - mistakes often incentivise change, welcoming new ideas. Frank Gehry illustrates this in his phases of prototyping, slicing and assembling strange shapes to an architectural configuration which is certainly *unique*..

{% figure caption: "Gehry and prototyping." caption%}.
<iframe width="680" height="315" src="https://www.youtube.com/embed/vYt2SQPqTh0?start=242" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
{% endfigure %}

Ariel stated an fascinating idea :

*"the repository of possible solutions"*

in our brain. The idea is that the brain stores a particular feature or element of a building or object we like, in order to solve a future design problem or simply for future design usage. It would be interesting to investigate the mind of a designer - are we more narrow minded - looking for quirky shapes with minimalistic colour schemes - or are we more open to radical ideas due to our design schooling?

### ethics

Design and philosophy intertwine. The discussion around ethics centred itself on analytical philosophy - on the recent news of the U.N secretary's statement of killer robots being [morally repugnant](https://www.nytimes.com/2018/10/19/technology/artificial-intelligence-weapons.html). We discussed utilitarian and consequentialist thinking, questioning who would be to blame if a self-driving car opted to kill the driver, rather than pedestrian if an accident were to happen? The ethics around AI and it's role in automated mechanisms is hugely debatable and incredibly interesting. What happens when a usually conscious choice is automated? Is the role of the professional **"to promote human flourishing?"** *(Ariel)*.

  My views differ here - design has historically promoted human flourishing, which has led to the manipulation of our environment and unequal distribution of resources. Perhaps the *purpose* of the professional has altered and there is a wider scope of human and non-human elements to be acknowledged, protect and promote.

## day 2 - [Sara de Ubieta](http://www.deubieta.com/)

![]({{site.baseurl}}/sara_collage.jpg)

In our first discussion with Sara, we presented ourselves and what we do. When Sara stated she was an architect-come-shoe-designer, we all consciously noted whether we were wearing a stylish pair of sneaks for the day.. It was interesting to hear that Sara had come from an architecture background, similar to *Jonathan* in Biology Zero, and many others on the course. Perhaps the architecture education provides a methodology for looking at the built environment, however had not quite fulfilled our desires to understand objects *a little bit more..* The economic crash had also affected the availability and need for architects, opening up new career explorations for those who found themselves in job instability.

Sara's primary craftmanship (or craftwomanship) in shoe design was the leather trade. Shadowing an expert, she learned the traditional methods of shoe-making. It was surprising and shocking to hear how much leather and materials were wasted in the shoe trade: something which Sara herself then decided to experiment with. Could shoes be made out of the scraps? How could the leather be worked to provide something superb and durable?

Many pairs of beautiful shoes were presented to us, and Sara reflected on her transition into factory production and it's regimented nature : factories do not **research**, but only **produce**. I was unaware of this design process, idealistically thinking that factories would have their own experimental teams which would test new materials in order to reduce the energy intensive process of shoe-making. In a nostalgic architectural momento, Sara created block wooden soles for her next line of shoes from tired wooden beams that would have been used for structural stability. Unless made aware, the average consumer would not be know that their shoes were actually something that provided structural purpose. A nice reuse I think - the beams transgressed from one structural support to another, for a smaller and more human sized foot.. The complexity and rigidity of the fashion industry is illustrated in Sara's persistent and plucky efforts to change the way that we design, produce and consume.

Moving away monoculture of leather, Sara has undertaken an experimental journey to explore how **new** materials can be worked and transformed : to our surprise, we then found ourselves in possession of shoes made from tiles, hair scrunchies and astroturf. My personal favourite had to be the 'faux leather' - fungi. The shoe may have resembled leather, however had none of the structural properties, being very spongey in feel and insatiably soft to the touch. The tangibility of materials can only be felt through a playful outlook, some scraps, glue and perhaps a sewing needle and thread. Which we proceeded to do. We were challenged to make a shoe from a sheet of insulation insulation, minimal material and our imagination. This was 'learning by doing' in action. Over 90 minutes, we became our very own shoe designers, questioning whether we would *ever* need to buy shoes again, when they can be made out of elements of our surrounding environment. The results were not quite *Louis Vuitton*, however our attitude and confidence towards experimentation had changed - we *really* believed in these shoes..

*"Understanding what occurs first in order to try out something experimental and new."*

(Sara)

Reflecting upon the session, Sara has provided inspiration to work persistently hard at what we believe in. There is a need to understand processes which currently occur occur in order to propose an intervention. My perception of *what* is considered as waste has changed again - similar to last week - what someone else does not want - we can use. Maybe I will not become a shoe designer, but I will strive to look at materials without predispositions and make time to test out their usability.

**What's on your feet, what's in your head?**


## day 3 - [Domestic Data Streamers](https://domesticstreamers.com/)

![]({{site.baseurl}}/dds_collage.jpg)

Our talk from Pol and Marta gave light to a whole new world of the beneficial usage of data. [Domestic Data Streamers](https://domesticstreamers.com/) (DDS) described themselves as a hybrid mix of social science and design: consisting of an interdisciplinary team that ranged from anthropologists to industrial designers - similar to our very own MDEF group. The name  'consultancy design firm' is not as exotic-sounding in comparison DDS's projects. DDS have an authentic, humanitarian approach: aiming to help communicate, share and understand the world we live in.

*"A more human approach."*

(DDS)

### using communication as a tool


Listing through their joint and independent projects, Pol and Marta reflected how it is important to address the misuse of data, and the stigma around data collection itself. There is a blurry world of data collection, through trackers, cookies and location services which I myself am unnerved by, therefore it was interesting to hear how collecting data can actually be used as a tool. The UNICEF 'Time Machine' project illustrated the potential possibilities for change with data collected for those who are often unheard. The projects which Pol and Marta undertook offer an inkling of activism, undressing this blurriness and ["making visible the invisible"](https://domesticstreamers.com/makingVisible/).


### what should design do ?


The recent ['Design Does'](https://designdoes.es/) exhibition, curated by DDS, questioned the role of design in society: for better or for worse. The exhibition created a platform on which topical, analytical questions could be posed to a mixed audience of designers and non-designers. The exhibition asked questions which many of us, including myself, are guilty of ignoring in our day-to-day lives, as it is easier to ignore a problem than address it. Chillingly accurate in it's questions regarding plastic, hidden production processes and human domination, Design Does addresses the need to change how we view and design in the world, to go out of our comfort zones and tackle address the problems we previously ignored. I am inspired by this provocative questioning, eager to start questioning processes myself.

Another favourite was 'Radio Me' - an uplifting project that embodies DDS's humanistic approach, looking at how simplified object interfaces can help communication between elderly people. It would be compelling to look further into this, into how we can create, improve or simplify technology to respond to ageing populations, loneliness and so on. It reminds me of Special Projects [Out of the Box](http://specialprojects.studio/project/out-of-the-box/).

Our final task required us to create our own data, from delving into our bags and finding objects which had meaning, a past, or a memory. I was a little stunned by how I could create data out of a piece of banana bread, a beanie, and a water bottle. Noticing some near-fatal dents on my water bottle, I swiftly opted for this in the hope it would provide me with some sort of knowledge I could translate into some form of visual data. Beginning to deface my water bottle with a (thankfully) semi-permanent marker, I began to understand what the set task was intending to do. In creating a simple chart on on how many times I had picked up the water bottle; how many litres drunk; *where* the water bottle had been and *what* had caused the alarming near-punctures, I created a rich surface in which rather amusingly reflected that my water bottle is with me more ofter than my friends, family or boyfriend.. There are many layers of meaning behind the exteriors that we see - similar to Design Does - we were exposing ourselves and the meaning of our objects, through offering a datascape reflecting our emotions, memories, places we had been, how long we have owned these objects and *why* : we were **making visible the invisible**.

## my identity

{% figure caption: "a collage of my current skills and desired skills." caption%}
![]({{site.baseurl}}/my identity collage.jpg)
{% endfigure %}

## my vision

My overarching vision as a designer is : to explore and question my role and actions as a designer in contemporary society and industry, through **experiencing challenging scenarios** which will inform my response to urban inequalities, waste and biological intricacy in a period of incessant growth.

If I were to explain the meaning of each word in my vision, I would have a very long sentence.. Therefore I have created a list of meanings :

Urban inequality: homelessness, biotic inequality, shared living, accessibility and knowledge of basic needs.

Waste: viewing waste differently, processing waste and using waste to create new products. Waste as a metaphor : seeing 'wasted' and 'waste' space as an opportunity.

Biological intricacy: understanding the micro to the macro scale - from fractals and self replication of bacteria to social cycles of the city.

Growth : developing the idea that we do not need continuous economic growth, looking at alternative systems that currently exist.

It is very easy to forget our current knowledge when participating in such varied activities, therefore the purpose of this vision is to develop my current knowledge of actor - network theory as a methodology for looking at loose space and push my understanding. I will utilise my interests in awareness, scale, urban loose space and urban permaculture to focus on increasing urban ecological inequalities in the hope that this has a boundless effect on quality of life and environment. This may seem a considerable challenge, however an element of optimism and belief is required, further, many of these systems are already functioning and exist, they are waiting to be *discovered*.

### what does this mean for me?

To question our automated processes and functions : do we really understand materials and why? Where do they come from? Explore the alternatives. *Listen* to others, how does my role as designer fit into this? What impact can designers have?

![]({{site.baseurl}}/thackra_1.jpg)

The rather inspirational 'How to Thrive in the Next Economy' by [John Thackara](https://www.goodreads.com/book/show/25690403-how-to-thrive-in-the-next-economy), provides fantastic examples on reasons to limit growth and explore the potential current, existing structures and cycles.

*"Natural time does not progress in straight lines; it moves in cycles that are shaped by the unique qualities of different locations.. there is no place in a world of pre-programmed growth for the complex temporality of plants, animals, and ecosystems."*

(p47)

This quote, I feel is applicable to the future designer. It is a request to embrace the chaos that may unfold in halting growth and extraction, to question and wonder at what is possible when we fuse the natural and unnatural, but to also have hope. There remains a cyclical nature of resurgence in the current habitual systems we have created. This resonates with the *1st person designer* (Heist, Tomico & Winthagen, 2012). The designer is therefore required to change societal outlook, *listening* and *responding* to a *need*. This is similar to [Designing for the Unknown](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products) - the designer is required to use reflective practice and understanding of resource vs capability. This is essential in future design conventionality : to understand the context of the matter and *what* the possibilities actually *are* in order to design within it.   


### If I were to sum myself in one word?




Playful.

I have often contested this part of my personality, questioning my optimism towards the future and how my current design practice tends to sway towards participation, using cultural probes and *people* to understand the world. However, after this week, I feel that I should augment this personality trait with the philosophies I am eager to learn. Being playful is a great way of creating new ideas : our personalised shoe making class changed opinions on material properties and flexed our rigid perceptions of design possibilities - the class stands as a metaphor for how I would like to approach design in the future. Being playful also allows a process of translation between the designer and user, allowing more intimate questions and ideas to be expressed. So, here's to new discoveries and becoming aware of new potential and possibilities.

{% figure caption: "*The Reinvention of Normal*, [Dominic Wilcox](http://dominicwilcox.com/)" caption%}
<iframe src="https://player.vimeo.com/video/122959827" width="640" height="360" frameborder="0" allowfullscreen></iframe>
{% endfigure %}




## food for thought

[Above&Below](https://www.itsnicethat.com/articles/studio-above-and-below-digital-160818)

[transient_space](https://www.itsnicethat.com/articles/transient-space-disrupt-the-channel-digital-191018)

[Monocle on Design. 'Why Materials Matter'](https://open.spotify.com/episode/2S2udnJ2O2bMTPvgvoo97v?si=By5u7rJ8RH-UwYGN9Bn4UQ)
