---
title: Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/doors.jpg)

![]({{site.baseurl}}/jess holding door.jpg)


Our mission of the week was to redesign the studio out of objects we found on the street. I say objects, as the items we found varied from : bone fragments, dismantled sunglasses, a rather fun looking slide which we unfortunately couldn't transport back via bike and a **lot** of beautiful wooden doors, wardrobes and chairs. The week was a great test of whether we, as designers, would be able to actually inhabit and use the space, and things we created.

## what is trash?

![]({{site.baseurl}}/google search.png)

I have a renewed outlook on what trash *actually is* - which is relative to a question of want and demand. Out first goal was to collect as many items which were salvageable, and then design our space with the objects in mind. Therefore, the item which was of no value to the original owner, now became valuable to us. Initially we searched Poblenou for items, however having a rich salvage economy, more industry and commerce than residences led us to explore other neighbourhoods. The affluent Sant Antoni became our next scavenging destination - it became apparent that this was no hidden secret - with small crowds lingering around bins waiting for 20:00 to strike. The physical process of trash hunting was clearly a pleasure shared by many others, and offered a glimpse of Barcelona's culture in which contrasts to that of the UK. If I were to stand by the bins waiting for people to put things out I would receive some strange looks and some offers for help even.

Call us trash activists.


## what do we need?

{% figure caption: "a render of the imagined 'coffee lab'" %}
![]({{site.baseurl}}/Coffee Lab.jpg)
{% endfigure %}

Divided into groups, we discussed the needs for the space and then presented our ideas to the class on a voting basis in which each individual selected their favourite aspects about each proposal. Selected to construct storage, movable walls to allow flexibility of space, bean bags (essential) and recycling, we pragmatically divided our team up to focus on select objects: I was to help with storage, bean bags and recycling. Instead of starting with 3d modelling, we selected various drawers, bits of wardrobe, and wooden planks, organising them on the floor and curating sketches in order to develop the best possible configuration before we began to build properly.

![]({{site.baseurl}}/stuffLab .jpg)
![]({{site.baseurl}}/working things out.jpg)
![]({{site.baseurl}}/storageCollage.jpg)
{% figure caption: "*design process : from render to physical actuality*" %}
![]({{site.baseurl}}/storage.jpg)
{% endfigure %}

## hacking

'Hacking' the materials meant looking at the **potential** of each object. How could we adjust, customise and tinker with each table, drawer and shelf to create a storage space which could act as a material library, a place to hang clothes and ensure our studio is tidy ?

The size and quality of materials differed: the drawers were slightly different in size and their inners made our of the rather delightfully difficult MDF. Our 'base' for the storage also lacked part of it's leg, it's wood splintered and a little riddled with (dead) woodworm. We rather lovingly adapted these pieces with wood scraps from the workshop, creating an unique but funky storage unit - coming to the newest hipster café near you..

![]({{site.baseurl}}/woodworm.jpg)

## learning process

- group discussion / participation
- presentation
- design collectives
- 3d modelling / illustrator
- prototyping
- CNC'ing the drawers, 3d printing a new leg cap for the base, hand tools (to complete project will use laser cutter + sewing machine)
- construction

![]({{site.baseurl}}/presentation.jpg)

## perceptions

Our estimation of how much we would be able to complete in the week was a little too high: working with mixed materials required a sensitivity and flexibility in our design approach. This meant that we were not able to complete the bean bags or recycling units, which are therefore work *in progress*. It was also surprisingly difficult to find polystyrene on the street, something which I thought would be in abundance. This therefore led us to reconsider the filling of the bean bags, perhaps using the shredder we would be able to use soft material to stuff them.  

![]({{site.baseurl}}/fifa + julia.jpg)

## a new life

The week not only provided us with a rather eloquent and unique space, but the process of the design and construction taught lessons around participation and working as a team. The changed perceptions we had were indicative of a circular movement which is beginning to take form on a larger scale - design studios such as [TransfoLAB](https://www.transfolabbcn.com/) provide aesthetically pleasing inspiration for why we should rethink what we see as waste. The 3d printer, CNC, laser cutter and sewing machine also increase the potential possibilities of reclamation and repair to repurpose furniture to create unique forms. Aspects we couldn't find led to questioning 'how could we adapt this using the technologies that we know ?' The 3d printed replacement cap for our storage unit is a charming and rather playful addition to our storage unit.

![]({{site.baseurl}}/table leg.jpg)

There is a further afterlife to the products we made, the storage units can be dismantled and the wood reused, they can also be separated into smaller freestanding components. Our plans for the recycling unit consist of measuring how much recycling we produce, and questioning whether we can reduce this or, for example, use our recycling cans as mini planters. We are also going to produce a simple open-source bean bag design in which others can download, and create more in their spare time. The week required us to be active participants in seeking **alternatives** to the easy, immediate solutions of the everyday and to question the systems which encourage the consume - dispose economy of the current world.

*"Our cities are currently designed for linear flow. Biological nutrients (such as food and wood) and technical nutrients (such as metals and plastics) enter at one end and are used, then discarded. After waste is sifted for valuable recyclables such as metals, paper and certain plastics, it flows out the other end, headed for landfills or incinerators. The process is “take, make, waste.” But just as we have redesigned certain consumer products to be disassembled, recycled or reused, we can design cities in a similar, circular fashion: take, make, retake, remake, restore."*

*(McDonoguh)*

![]({{site.baseurl}}/fifa .jpg)

## food for thought

[ecoBirdy](https://www.ecobirdy.com/)

[Polimeer Amsterdam](http://www.polimeer.com/en/)

[Nukak](https://www.nukak.es/en/content/29-sobre-nosotros2)

[social furniture](http://www.eoos.com/cms/index.php?id=353)

[William McDonoguh, author of Cradle to Cradle](http://mcdonoughpartners.com/william-mcdonough-scientific-american-designing-city-tomorrow/)
