---
title: Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---
![]({{site.baseurl}}/teaching.jpg)

Designing with Extended Intelligence highlighted many outstanding complexities and topical questions which have resided through the existence of human intellectual ponderings, based on the idea that, in order to *design* intelligence, we must know what it first *is*. Perhaps when you visualise 'intelligence' you see academics suited in tweed relaxing in a literary-clad office, debating existential questions over a tumbler of whisky. I don't think you would have imagined a computer, data, or known about the idea of 'collective intelligence'. Collective intelligence is an incredibly powerful idea that many individual thought processes, combined, create a more powerful network than a singular person alone. MIT even have a ['Center for Collective Intelligence'](https://cci.mit.edu/) - dedicated to pursuing the idea that humanities great ideas can be connected and networked technologically to address societal changes. Our concept of intelligence requires a change from it's humanistic bias, to acknowledge other entities. The [Anthropocene Magazine](http://www.anthropocenemagazine.org/) is an inspiring case of positivism and excitement towards innovation for the future.

*"If the world is going to understand science, it's not going to be because they've read a scientific article, it's going to be because we tell compelling stories, have conversations."*

**So, how does this embed into what we already know?**

<figure>

<iframe width="650" height="360" src="https://www.youtube.com/embed/aza461g7-Mc?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<figcaption>The 'Anthropocene Magazine' - designing for the present.</figcaption>
</figure>


## how much do we *actually* know?

The idea of collective intelligence designing innovative platforms for the future is fantastic, *intelligent* even. However, through group discussions of designing our very own intelligent object led to the realisation that, what we know, or what we *think* we know, is relatively very little to the time scale humans have existed on earth - the definition of intelligence is a question which will consistently be debated due to its subjective application to different topical areas.
The rapid technological advancement over the past century is an impressive feat - I myself come from the generation in which experienced the 'life-before-mobiles' - in which now, phones are an extended interface of our bodies. It's therefore not surprising to hear anecdotal pre-phone references from our elders.. This is also indicative of issues of translatability of new technologies, an issue that needs probed further. If we are going to 'design for emergent futures', we need to design for a future inclusive of people who are technology skeptics and novices. The project below follows the path of a 'Smart Fork' which, *in theory*, is great. This wonder-fork measures your nutritional intake per-mouthful, sending you sinful reminders of over-indulgence..

<figure>

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/128873380" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<figcaption>'Uninvited Guests' by Superflux.</figcaption>
</figure>

## data

Data has now become a consistent presence in our environment today, it is used to feed systems, behaviours and spending. Our data is spread and sold in milliseconds due to our accessible online presence - how is it that *privacy* has now become a luxury? Many of us, including myself, do not realise how far and wide this data has been used. 'Leave.EU' campaign is now facing a hefty fine over [data misuse](https://www.theguardian.com/uk-news/2018/nov/06/arron-banks-firm-and-leave-eu-face-135k-fine-over-data-misuse) - just one example of how the inscriptions of our daily lives can feed a political mechanism. Data is not always negative however : data surrounds us, it is a translation of our inputs into the world, not solely a stream of numbers on a screen. Data representation can express our innermost opinions on a matter.

If data is the essence of how we translate our daily processes into meaning, then feeding data into an intelligent object would never be an easy task. The incredible complexity of our own complex behavioural systems, and sometimes *irrationality* is rather difficult to objectify in order for a machine to comprehend. How can we explain to a machine that we indulge in food little bit too much around Christmas despite the fact we know it is not good for our health? Perhaps we need some form of feedback from our intelligent objects which remind us of our glutinous rebellions with a dry humour.  The Smart Fork embodies this - in the video above - brilliantly designed to take the fun out of your luncheons.



## what does it mean to be a human

So, what does it mean to be human? Can we capture this in order to translate this meaning for intelligent forms to then replicate? This could be taken literally, as humans we are physically present, a complex set of organs, bones and tissues which roam, experiencing the tangibility of day-to-day life, forgetting many of our actions which have led to irreversible changes in humanity and ultimately, the planet. Current attempts to recreate humanoid figures have led to the 'uncanny valley' effect: the figure presented to the audience looks *almost* like a human, but lacking something..
The beautiful complexity of humanity however lies in our cognitions. Currently, we view humanity as unique through the ability to perform consciousness and sentience. Machines have already shown us that they *will* take over our jobs: they are far more effective, and with far larger memory systems than we could ever currently implement. However, machines are currently unable to *feel* those irrational emotions that make us what we are: machines cannot explain why we perform some of the actions we do, even *we* cannot explain them.

*"But nobody would ever ask a machine what it thinks about machines that think. It's a question that only makes sense if we care about the thinker as an autonomous and interesting being like ourselves. If somebody ever does ask a machine this question, it won't be a machine any more. I think I'm not going to worry about it for a while. You may think I'm in denial."*

[WHAT DO YOU THINK ABOUT MACHINES THAT THINK?](https://www.edge.org/responses/q2015)



## should AI *be* human?

The scene from *I, Robot* comes to mind here, in which the robots have rebelled against humanity, yet there is one robotic saviour which developed sentience. *I, Robot* does discuss the issue of consciousness in AI, however illustrates the main flaw when we think about AI. Often, when you mention AI, we think of a robotic humanoid figure which will clean your house, filling you in on your schedule for the day whilst handing you car keys as you amble out of the door for a day's work. The most advanced AI currently resides in the AI in which we do not know exists and does not resemble human form - this is because it is effective at its job. For example, Google Search is a (slightly sexist) AI algorhythm, it exists in hoovers, fridges and so on. The data we feed these machines is data *we* have created - it will reflect this data back to us.
*Can* we create an object which will surpass humanity? Is this the ultimate human sacrifice that we may or may not have the choice to make? This is an interesting question to propose - questioning our status on the planet: do we *deserve* to destroy a possible future or will it happen anyway?



## robots are not going to take over the world, yet

![]({{site.baseurl}}/ML.png)



The image above is [Marrow's](https://marrow.raycaster.studio/) AI text-to-image generator. This, rather comically, produces an image, that does not represent *a woman trying to write what she thinks about machine learning.* If I were to imagine 'a woman trying to think what she thinks about machine learning', I would maybe also not know what to create, but it wouldn't be the creepy blending of images that slightly resemble the figure above.

How about something more simple?



![]({{site.baseurl}}/apple.png)



Something still not quite right here.



The text-to-image generator, produced by [Marrow](https://marrow.raycaster.studio/) is an example of machine learning attempting to create an image through text input. Marrow is an interesting project, looking at mental health and AI - whether machines can be motivated by empathy and compassion, *towards another machine.* This is reflective of an 'emergent weirdness' in AI. It sounds like an illegitimate phrase to describe the complex jargon of the AI world, however aptly describes the difficulty in objectifying complex behaviours to data. Can machines understand the concept of chaos? 'Emergent weirdness' highlights the **shortcuts** the human brains take to understand our daily life. These shortcuts feed into behavioural patterns, which are influenced by scales and forces that we do not (yet) detect, or examine. When AI attempts to replicate these shortcuts is when these slightly kooky elements are produced. Why is it that we are even trying to reproduce human behaviours anyway? Are we not feeding into a self-replicating, or *self-destructing* desire of mastery? What if we were to design intelligence to reveal our complex behaviours, but also the relationships between fundamentals which we overlook?

*"Machine learning technology demands that we master nature; it compels us to falsely believe that we can perfectly model human behaviour, desires, and motivation."*

[Immerse, 'When Machines Look for Order in Chaos'](https://marrow.raycaster.studio/).

*Designing with Intelligence* questioned our **awareness** of the world, using contemporary technologies as a tool to develop our knowledge of the complexities which exist in designing intelligent objects. It has also reflected on the amazing capacities of design. Designing for the future, or even for the present contains an element of speculation — what is our *new reality?*



## food for thought

[inspirobot](http://inspirobot.me) : endless, useless inspiration and weird AI quotes to stimulate your daily amusement. I would recommend the 'mindfulness' area.


[The 5th Dimensional Camera.](http://superflux.in/index.php/work/5th-dimensional-camera/#)


[Emergent Weirdness in AI.](https://www.theatlantic.com/technology/archive/2011/11/daniel-kahneman-on-emergent-weirdness-in-artifical-intelligences/249125/)


[Center for Humane Technology.](http://humanetech.com/)


[The Human Factor in AI.](https://www.chathamhouse.org/expert/comment/human-factor-essential-eliminating-bias-artificial-intelligence#)


[The Case for Fairer Algorithms](https://medium.com/@Ethics_Society/the-case-for-fairer-algorithms-c008a12126f8)
